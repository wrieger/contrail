use crate::mem::Block;

/// A pointer to a single block of memory.
#[derive(Clone, Copy)]
pub struct RawPointer {
    offset: usize,
}

impl RawPointer {
    fn new(offset: usize) -> Self {
        Self { offset }
    }

    #[inline(always)]
    pub fn get(&self, memory: &Memory) -> Block {
        memory.blocks[self.offset]
    }

    #[inline(always)]
    pub fn set(&self, memory: &mut Memory, block: Block) {
        memory.blocks[self.offset] = block;
    }
}

/// A pointer to a contiguous array of blocks of memory.
#[derive(Clone, Copy)]
pub struct RawArrayPointer {
    index: usize,
    len: usize,
}

impl RawArrayPointer {
    fn new(index: usize, len: usize) -> Self {
        Self { index, len }
    }

    #[inline(always)]
    pub fn len(&self) -> usize {
        self.len
    }

    #[inline(always)]
    pub fn get(&self, memory: &Memory, index: usize) -> Block {
        assert!(index < self.len);
        memory.blocks[self.index + index]
    }

    #[inline(always)]
    pub fn set(&self, memory: &mut Memory, index: usize, block: Block) {
        assert!(index < self.len);
        memory.blocks[self.index + index] = block;
    }

    #[inline(always)]
    pub fn swap(&self, memory: &mut Memory, i: usize, j: usize) {
        assert!(i < self.len);
        assert!(j < self.len);
        memory.blocks.swap(self.index + i, self.index + j);
    }
}

pub struct MemoryBuilder {
    blocks: Vec<Block>,
}

impl MemoryBuilder {
    /// Creates a new `MemoryBuilder`.
    ///
    /// # Examples
    ///
    /// ```
    /// use contrail::mem::raw::MemoryBuilder;
    ///
    /// let mut builder = MemoryBuilder::new();
    /// ```
    pub fn new() -> Self {
        Self { blocks: vec![] }
    }

    /// Adds the block to the memory, returning a pointer to the block.
    ///
    /// # Examples
    ///
    /// ```
    /// use contrail::mem::raw::MemoryBuilder;
    ///
    /// let mut builder = MemoryBuilder::new();
    /// let pointer = builder.add_block(42);
    /// ```
    pub fn add_block(&mut self, block: Block) -> RawPointer {
        let index = self.blocks.len();
        self.blocks.push(block);
        RawPointer::new(index)
    }

    /// Adds an array of blocks to the memory, returning a pointer to the array.
    ///
    /// # Examples
    ///
    /// ```
    /// use contrail::mem::raw::MemoryBuilder;
    ///
    /// let mut builder = MemoryBuilder::new();
    /// let pointer = builder.add_block_array(10..20);
    /// ```
    pub fn add_block_array(&mut self, blocks: impl IntoIterator<Item = Block>) -> RawArrayPointer {
        let index = self.blocks.len();
        let mut len = 0;
        for block in blocks {
            len += 1;
            self.blocks.push(block);
        }
        RawArrayPointer::new(index, len)
    }

    pub fn finish(self) -> Memory {
        Memory {
            blocks: self.blocks,
        }
    }
}

/// A struct to store blocks.
#[derive(Clone)]
pub struct Memory {
    blocks: Vec<Block>,
}
