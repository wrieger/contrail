use crate::mem::Block;

/// Utility trait for converting to and from a type `T`.
pub trait Convert<T> {
    /// Converts `T` into `Self`.
    fn convert_from(val: T) -> Self;

    /// Converts `Self` into `T`.
    fn convert_into(self) -> T;
}

macro_rules! impl_convert_one_block_integer {
    ( [ $( $T:ty ),* ] ) => {
        $(
            impl Convert<[Block; 1]> for $T {
                #[inline(always)]
                fn convert_from(blocks: [Block; 1]) -> Self {
                    blocks[0] as $T
                }

                #[inline(always)]
                fn convert_into(self) -> [Block; 1] {
                    [(self as Block)]
                }
            }
        )*
    }
}

macro_rules! impl_convert_two_block_integer {
    ( [ $( $T:ty ),* ] ) => {
        $(
            impl Convert<[Block; 2]> for $T {
                #[inline(always)]
                fn convert_from(blocks: [Block; 2]) -> Self {
                    let low = blocks[0] as $T;
                    let high = blocks[1] as $T;
                    let b_bit_len = 8 * std::mem::size_of::<Block>();
                    (high << b_bit_len) + low
                }

                #[inline(always)]
                fn convert_into(self) -> [Block; 2] {
                    let low = self as Block;
                    let b_bit_len = 8 * std::mem::size_of::<Block>();
                    let high = (self >> b_bit_len) as Block;
                    [low, high]
                }
            }
        )*
    };
}

impl Convert<[Block; 1]> for bool {
    fn convert_from(blocks: [Block; 1]) -> Self {
        blocks[0] != 0
    }

    fn convert_into(self) -> [Block; 1] {
        if self {
            [1]
        } else {
            [0]
        }
    }
}

macro_rules! impl_convert_option {
    ( $( $n:expr ),* ) => {
        $(
            impl<T> Convert<[Block; $n + 1]> for Option<T>
            where
                T: Convert<[Block; $n]>,
            {
                fn convert_from(blocks: [Block; $n + 1]) -> Self {
                    let is_some = bool::convert_from([blocks[0]]);
                    if is_some {
                        let mut t_blocks = [0; $n];
                        t_blocks.copy_from_slice(&blocks[1..]);
                        Some(T::convert_from(t_blocks))
                    } else {
                        None
                    }
                }

                fn convert_into(self) -> [Block; $n + 1] {
                    if let Some(t) = self {
                        let mut blocks = [0; $n + 1];
                        blocks[0] = 1;
                        blocks[1..].copy_from_slice(&t.convert_into());
                        blocks
                    } else {
                        [0; $n + 1]
                    }
                }
            }
        )*
    }
}

cfg_if! {
    if #[cfg(feature = "block-u32")] {
        impl_convert_one_block_integer!([i32, u32]);

        impl_convert_two_block_integer!([i64, u64]);

        #[cfg(target_pointer_width = "32")]
        impl_convert_one_block_integer!([isize, usize]);

        #[cfg(target_pointer_width = "64")]
        impl_convert_two_block_integer!([isize, usize]);
    } else {
        impl_convert_one_block_integer!([i32, u32, i64, u64, isize, usize]);

        impl_convert_two_block_integer!([i128, u128]);
    }
}

impl_convert_option!(1, 2, 3, 4, 5, 6, 7, 8, 9);
