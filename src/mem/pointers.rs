use std::marker::PhantomData;

use crate::mem::raw::Memory;
use crate::mem::raw::MemoryBuilder;
use crate::mem::raw::RawArrayPointer;
use crate::mem::raw::RawPointer;
use crate::mem::util::Convert;
use crate::mem::ArrayPointer;
use crate::mem::Block;
use crate::mem::Pointer;

pub trait TestPointer {
    fn from_blocks(blocks: &[Block]) -> Self;
    fn into_blocks(blocks: &mut [Block]);
}

/// A pointer to a type that takes up a single block of memory.
pub struct OneBlockPointer<T> {
    pointer: RawPointer,
    phantom: PhantomData<T>,
}

impl<T: Convert<[Block; 1]>> Pointer for OneBlockPointer<T> {
    type Val = T;

    fn new(memory: &mut MemoryBuilder, val: T) -> Self {
        Self {
            pointer: memory.add_block(val.convert_into()[0]),
            phantom: PhantomData,
        }
    }

    #[inline(always)]
    fn get(&self, memory: &Memory) -> T {
        T::convert_from([self.pointer.get(memory)])
    }

    #[inline(always)]
    fn set(&self, memory: &mut Memory, val: T) {
        self.pointer.set(memory, val.convert_into()[0]);
    }
}

impl<T> Clone for OneBlockPointer<T> {
    fn clone(&self) -> Self {
        Self {
            pointer: self.pointer,
            phantom: PhantomData,
        }
    }
}

impl<T> Copy for OneBlockPointer<T> {}

/// A pointer to a type that takes up two blocks of memory.
pub struct TwoBlockPointer<T> {
    low_pointer: RawPointer,
    high_pointer: RawPointer,
    phantom: PhantomData<T>,
}

impl<T: Convert<[Block; 2]>> Pointer for TwoBlockPointer<T> {
    type Val = T;

    fn new(memory: &mut MemoryBuilder, val: T) -> Self {
        let two_blocks = val.convert_into();
        let low_pointer = memory.add_block(two_blocks[0]);
        let high_pointer = memory.add_block(two_blocks[1]);
        Self {
            low_pointer,
            high_pointer,
            phantom: PhantomData,
        }
    }

    #[inline(always)]
    fn get(&self, memory: &Memory) -> T {
        let low = self.low_pointer.get(memory);
        let high = self.high_pointer.get(memory);
        T::convert_from([low, high])
    }

    #[inline(always)]
    fn set(&self, memory: &mut Memory, val: T) {
        let two_blocks = val.convert_into();
        self.low_pointer.set(memory, two_blocks[0]);
        self.high_pointer.set(memory, two_blocks[1]);
    }
}

impl<T> Clone for TwoBlockPointer<T> {
    fn clone(&self) -> Self {
        Self {
            low_pointer: self.low_pointer,
            high_pointer: self.high_pointer,
            phantom: PhantomData,
        }
    }
}

impl<T> Copy for TwoBlockPointer<T> {}

/// A pointer to an array of types that each take up one block of memory.
pub struct OneBlockArrayPointer<T> {
    pointer: RawArrayPointer,
    phantom: PhantomData<T>,
}

impl<T: Convert<[Block; 1]>> ArrayPointer for OneBlockArrayPointer<T> {
    type Val = T;

    fn new(memory: &mut MemoryBuilder, vals: impl IntoIterator<Item = T>) -> Self {
        Self {
            pointer: memory.add_block_array(vals.into_iter().map(|t| t.convert_into()[0])),
            phantom: PhantomData,
        }
    }

    #[inline(always)]
    fn len(&self) -> usize {
        self.pointer.len()
    }

    #[inline(always)]
    fn get(&self, memory: &Memory, index: usize) -> T {
        T::convert_from([self.pointer.get(memory, index)])
    }

    #[inline(always)]
    fn set(&self, memory: &mut Memory, index: usize, val: T) {
        self.pointer.set(memory, index, val.convert_into()[0]);
    }

    #[inline(always)]
    fn swap(&self, memory: &mut Memory, i: usize, j: usize) {
        self.pointer.swap(memory, i, j);
    }
}

impl<T> Clone for OneBlockArrayPointer<T> {
    fn clone(&self) -> Self {
        Self {
            pointer: self.pointer,
            phantom: PhantomData,
        }
    }
}

impl<T> Copy for OneBlockArrayPointer<T> {}

/// A pointer to an array of values that each take up a two blocks of memory.
pub struct TwoBlockArrayPointer<T> {
    pointer: RawArrayPointer,
    phantom: PhantomData<T>,
}

impl<T: Convert<[Block; 2]>> ArrayPointer for TwoBlockArrayPointer<T> {
    type Val = T;

    fn new(memory: &mut MemoryBuilder, vals: impl IntoIterator<Item = T>) -> Self {
        Self {
            pointer: memory.add_block_array(
                vals.into_iter()
                    .map(|t| {
                        let [low, high] = t.convert_into();
                        std::iter::once(low).chain(std::iter::once(high))
                    })
                    .flatten(),
            ),
            phantom: PhantomData,
        }
    }

    fn len(&self) -> usize {
        self.pointer.len() / 2
    }

    #[inline(always)]
    fn get(&self, memory: &Memory, index: usize) -> T {
        let mem_index = 2 * index;
        let low = self.pointer.get(memory, mem_index);
        let high = self.pointer.get(memory, mem_index + 1);
        T::convert_from([low, high])
    }

    #[inline(always)]
    fn set(&self, memory: &mut Memory, index: usize, val: T) {
        let mem_index = 2 * index;
        let [low, high] = val.convert_into();
        self.pointer.set(memory, mem_index, low);
        self.pointer.set(memory, mem_index + 1, high);
    }

    #[inline(always)]
    fn swap(&self, memory: &mut Memory, i: usize, j: usize) {
        let mem_i = 2 * i;
        let mem_j = 2 * j;
        self.pointer.swap(memory, mem_i, mem_j);
        self.pointer.swap(memory, mem_i + 1, mem_j + 1);
    }
}

impl<T> Clone for TwoBlockArrayPointer<T> {
    fn clone(&self) -> Self {
        Self {
            pointer: self.pointer,
            phantom: PhantomData,
        }
    }
}

impl<T> Copy for TwoBlockArrayPointer<T> {}
