pub mod pointers;
pub mod raw;
pub mod util;

cfg_if! {
    if #[cfg(feature = "block-u32")] {
        pub type Block = u32;
    } else {
        pub type Block = u64;
    }
}

use crate::mem::raw::Memory;
use crate::mem::raw::MemoryBuilder;

/// A generic pointer to a value in memory.
pub trait Pointer: Copy {
    type Val;

    fn new(memory: &mut MemoryBuilder, val: Self::Val) -> Self;
    fn get(&self, memory: &Memory) -> Self::Val;
    fn set(&self, memory: &mut Memory, val: Self::Val);
}

/// A generic pointer to an array of values in memory.
pub trait ArrayPointer: Copy {
    type Val;

    fn new(memory: &mut MemoryBuilder, vals: impl IntoIterator<Item = Self::Val>) -> Self;
    fn len(&self) -> usize;
    fn get(&self, memory: &Memory, i: usize) -> Self::Val;
    fn set(&self, memory: &mut Memory, i: usize, val: Self::Val);
    fn swap(&self, memory: &mut Memory, i: usize, j: usize);
}
