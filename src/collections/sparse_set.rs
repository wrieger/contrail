use crate::collections::Value;
use crate::StoredUsizeArray;
use crate::Trail;
use crate::TrailBuilder;
use crate::TrailedUsize;

#[derive(Clone, Copy)]
pub struct TrailedSparseSet {
    values: StoredUsizeArray,
    positions: StoredUsizeArray,
    len: TrailedUsize,
}

impl TrailedSparseSet {
    pub fn new_full(builder: &mut TrailBuilder, len: usize) -> Self {
        Self {
            values: StoredUsizeArray::new(builder, 0..len),
            positions: StoredUsizeArray::new(builder, 0..len),
            len: TrailedUsize::new(builder, len),
        }
    }

    #[inline(always)]
    pub fn iter<'s, 't: 's>(&'s self, trail: &'t Trail) -> impl Iterator<Item = Value> + 's {
        (0..self.values.len())
            .map(move |i| self.values.get(trail, i))
            .take(self.len.get(trail))
            .map(Value)
    }

    #[inline(always)]
    pub fn len(&self, trail: &Trail) -> usize {
        self.len.get(trail)
    }

    #[inline(always)]
    pub fn is_empty(&self, trail: &Trail) -> bool {
        self.len.get(trail) == 0
    }

    #[inline(always)]
    pub fn contains(&self, trail: &Trail, val: Value) -> bool {
        val.0 < self.positions.len() && self.positions.get(trail, val.0) < self.len.get(trail)
    }

    #[inline(always)]
    pub fn remove(&self, trail: &mut Trail, val: Value) {
        if self.contains(trail, val) {
            let position = self.positions.get(trail, val.0);
            let new_size = self.len.get(trail) - 1;
            self.swap(trail, position, new_size);
            self.len.set(trail, new_size);
        }
    }

    #[inline(always)]
    pub fn intersect(&self, trail: &mut Trail, vals: impl IntoIterator<Item = Value>) {
        let mut new_size = 0;
        for val in vals {
            if self.contains(trail, val) {
                let position = self.positions.get(trail, val.0);
                self.swap(trail, position, new_size);
                new_size += 1;
            }
        }
        self.len.set(trail, new_size);
    }

    #[inline(always)]
    fn swap(&self, trail: &mut Trail, i: usize, j: usize) {
        let val_i = self.values.get(trail, i);
        let val_j = self.values.get(trail, j);

        self.values.set(trail, i, val_j);
        self.values.set(trail, j, val_i);

        self.positions.set(trail, val_i, j);
        self.positions.set(trail, val_j, i);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use crate::TrailBuilder;

    #[test]
    fn test() {
        let mut builder = TrailBuilder::new();

        // 0..5
        let trailed_sparse_set = TrailedSparseSet::new_full(&mut builder, 5);

        let trail = &mut builder.finish();

        trail.new_level();

        trailed_sparse_set.remove(trail, Value(1));
        assert!(!trailed_sparse_set.contains(trail, Value(1)));

        trail.new_level();

        trailed_sparse_set.remove(trail, Value(4));
        assert!(!trailed_sparse_set.contains(trail, Value(4)));

        trailed_sparse_set.remove(trail, Value(2));

        assert!(!trailed_sparse_set.contains(trail, Value(4)));

        trail.new_level();

        trailed_sparse_set.remove(trail, Value(0));
        assert!(!trailed_sparse_set.contains(trail, Value(0)));

        trail.undo_level();

        assert!(trailed_sparse_set.contains(trail, Value(0)));

        trail.undo_level();

        assert!(trailed_sparse_set.contains(trail, Value(4)));
        assert!(trailed_sparse_set.contains(trail, Value(2)));

        trail.undo_level();

        assert!(trailed_sparse_set.contains(trail, Value(1)));
    }
}
