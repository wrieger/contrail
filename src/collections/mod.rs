//! Collections built out of trailed and stored primitives.
mod bit_set;
mod sparse_set;
mod linked_list;

pub use self::bit_set::StoredBitSet;
pub use self::bit_set::TrailedBitSet;
pub use self::sparse_set::TrailedSparseSet;

#[derive(Clone, Copy)]
pub struct Value(pub usize);
