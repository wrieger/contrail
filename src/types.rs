cfg_if! {
    if #[cfg(feature = "block-u32")] {
        use crate::mem::pointers::OneBlockArrayPointer;
        use crate::mem::pointers::OneBlockPointer;
        use crate::mem::pointers::TwoBlockArrayPointer;
        use crate::mem::pointers::TwoBlockPointer;
        use crate::StoredArray;
        use crate::StoredValue;
        use crate::TrailedArray;
        use crate::TrailedValue;

        pub type TrailedI32 = TrailedValue<OneBlockPointer<i32>>;
        pub type TrailedI64 = TrailedValue<TwoBlockPointer<i64>>;
        #[cfg(target_pointer_width = "32")]
        pub type TrailedIsize = Trailed<OneBlockPointer<isize>>;
        #[cfg(target_pointer_width = "64")]
        pub type TrailedIsize = TrailedValue<TwoBlockPointer<isize>>;

        pub type TrailedU32 = TrailedValue<OneBlockPointer<u32>>;
        pub type TrailedU64 = TrailedValue<TwoBlockPointer<u64>>;
        #[cfg(target_pointer_width = "32")]
        pub type TrailedUsize = Trailed<OneBlockPointer<usize>>;
        #[cfg(target_pointer_width = "64")]
        pub type TrailedUsize = TrailedValue<TwoBlockPointer<usize>>;

        pub type StoredI32 = StoredValue<OneBlockPointer<i32>>;
        pub type StoredI64 = StoredValue<TwoBlockPointer<i64>>;
        #[cfg(target_pointer_width = "32")]
        pub type StoredIsize = StoredValue<OneBlockPointer<isize>>;
        #[cfg(target_pointer_width = "64")]
        pub type StoredIsize = StoredValue<TwoBlockPointer<isize>>;

        pub type StoredU32 = StoredValue<OneBlockPointer<u32>>;
        pub type StoredU64 = StoredValue<TwoBlockPointer<u64>>;
        #[cfg(target_pointer_width = "32")]
        pub type StoredUsize = StoredValue<OneBlockPointer<usize>>;
        #[cfg(target_pointer_width = "64")]
        pub type StoredUsize = StoredValue<TwoBlockPointer<usize>>;

        pub type TrailedI32Array = TrailedArray<OneBlockArrayPointer<i32>>;
        pub type TrailedI64Array = TrailedArray<TwoBlockArrayPointer<i64>>;
        #[cfg(target_pointer_width = "32")]
        pub type TrailedIsizeArray = TrailedArray<OneBlockArrayPointer<isize>>;
        #[cfg(target_pointer_width = "64")]
        pub type TrailedIsizeArray = TrailedArray<TwoBlockArrayPointer<isize>>;

        pub type TrailedU32Array = TrailedArray<OneBlockArrayPointer<u32>>;
        pub type TrailedU64Array = TrailedArray<TwoBlockArrayPointer<u64>>;
        #[cfg(target_pointer_width = "32")]
        pub type TrailedUsizeArray = TrailedArray<OneBlockArrayPointer<usize>>;
        #[cfg(target_pointer_width = "64")]
        pub type TrailedUsizeArray = TrailedArray<TwoBlockArrayPointer<usize>>;

        pub type StoredI32Array = StoredArray<OneBlockArrayPointer<i32>>;
        pub type StoredI64Array = StoredArray<TwoBlockArrayPointer<i64>>;
        #[cfg(target_pointer_width = "32")]
        pub type StoredIsizeArray = StoredArray<OneBlockArrayPointer<isize>>;
        #[cfg(target_pointer_width = "64")]
        pub type StoredIsizeArray = StoredArray<TwoBlockArrayPointer<isize>>;

        pub type StoredU32Array = StoredArray<OneBlockArrayPointer<u32>>;
        pub type StoredU64Array = StoredArray<TwoBlockArrayPointer<u64>>;
        #[cfg(target_pointer_width = "32")]
        pub type StoredUsizeArray = StoredArray<OneBlockArrayPointer<usize>>;
        #[cfg(target_pointer_width = "64")]
        pub type StoredUsizeArray = StoredArray<TwoBlockArrayPointer<usize>>;
    } else {
        use crate::mem::pointers::OneBlockArrayPointer;
        use crate::mem::pointers::OneBlockPointer;
        use crate::StoredArray;
        use crate::StoredValue;
        use crate::TrailedArray;
        use crate::TrailedValue;

        pub type TrailedI32 = TrailedValue<OneBlockPointer<i32>>;
        pub type TrailedI64 = TrailedValue<OneBlockPointer<i64>>;
        pub type TrailedIsize = TrailedValue<OneBlockPointer<isize>>;

        pub type TrailedU32 = TrailedValue<OneBlockPointer<u32>>;
        pub type TrailedU64 = TrailedValue<OneBlockPointer<u64>>;
        pub type TrailedUsize = TrailedValue<OneBlockPointer<usize>>;

        pub type StoredI32 = StoredValue<OneBlockPointer<i32>>;
        pub type StoredI64 = StoredValue<OneBlockPointer<i64>>;
        pub type StoredIsize = StoredValue<OneBlockPointer<isize>>;

        pub type StoredU32 = StoredValue<OneBlockPointer<u32>>;
        pub type StoredU64 = StoredValue<OneBlockPointer<u64>>;
        pub type StoredUsize = StoredValue<OneBlockPointer<usize>>;

        pub type TrailedI32Array = TrailedArray<OneBlockArrayPointer<i32>>;
        pub type TrailedI64Array = TrailedArray<OneBlockArrayPointer<i64>>;
        pub type TrailedIsizeArray = TrailedArray<OneBlockArrayPointer<isize>>;

        pub type TrailedU32Array = TrailedArray<OneBlockArrayPointer<u32>>;
        pub type TrailedU64Array = TrailedArray<OneBlockArrayPointer<u64>>;
        pub type TrailedUsizeArray = TrailedArray<OneBlockArrayPointer<usize>>;

        pub type StoredI32Array = StoredArray<OneBlockArrayPointer<i32>>;
        pub type StoredI64Array = StoredArray<OneBlockArrayPointer<i64>>;
        pub type StoredIsizeArray = StoredArray<OneBlockArrayPointer<isize>>;

        pub type StoredU32Array = StoredArray<OneBlockArrayPointer<u32>>;
        pub type StoredU64Array = StoredArray<OneBlockArrayPointer<u64>>;
        pub type StoredUsizeArray = StoredArray<OneBlockArrayPointer<usize>>;
    }
}
