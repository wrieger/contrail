#![feature(test)]

#[macro_use]
extern crate cfg_if;

pub mod collections;
pub mod mem;
pub mod types;

pub use self::types::*;

use crate::mem::raw::Memory;
use crate::mem::raw::MemoryBuilder;
use crate::mem::ArrayPointer;
use crate::mem::Pointer;

/// The trail itself.
pub struct Trail {
    trailed_mem: Memory,
    stored_mem: Memory,
    trail: Vec<Memory>,
}

impl Trail {
    pub fn new_level(&mut self) {
        self.trail.push(self.trailed_mem.clone());
    }

    pub fn undo_level(&mut self) {
        if let Some(prev) = self.trail.pop() {
            self.trailed_mem = prev;
        }
    }
}

pub struct TrailBuilder {
    trailed_mem: MemoryBuilder,
    stored_mem: MemoryBuilder,
}

impl TrailBuilder {
    pub fn new() -> Self {
        Self {
            trailed_mem: MemoryBuilder::new(),
            stored_mem: MemoryBuilder::new(),
        }
    }

    pub fn finish(self) -> Trail {
        Trail {
            trailed_mem: self.trailed_mem.finish(),
            stored_mem: self.stored_mem.finish(),
            trail: vec![],
        }
    }

    pub fn finish_with_capacity(self, cap: usize) -> Trail {
        Trail {
            trailed_mem: self.trailed_mem.finish(),
            stored_mem: self.stored_mem.finish(),
            trail: Vec::with_capacity(cap),
        }
    }
}

#[derive(Clone, Copy)]
pub struct TrailedValue<P> {
    pointer: P,
}

impl<P> TrailedValue<P>
where
    P: Pointer,
{
    pub fn new(builder: &mut TrailBuilder, val: P::Val) -> Self {
        TrailedValue {
            pointer: P::new(&mut builder.trailed_mem, val),
        }
    }

    #[inline(always)]
    pub fn get(&self, trail: &Trail) -> P::Val {
        self.pointer.get(&trail.trailed_mem)
    }

    #[inline(always)]
    pub fn set(&self, trail: &mut Trail, val: P::Val) {
        self.pointer.set(&mut trail.trailed_mem, val);
    }
}

#[derive(Clone, Copy)]
pub struct TrailedArray<P> {
    pointer: P,
}

impl<P> TrailedArray<P>
where
    P: ArrayPointer,
{
    pub fn new(builder: &mut TrailBuilder, vals: impl IntoIterator<Item = P::Val>) -> Self {
        TrailedArray {
            pointer: P::new(&mut builder.trailed_mem, vals),
        }
    }

    #[inline(always)]
    pub fn len(&self) -> usize {
        self.pointer.len()
    }

    #[inline(always)]
    pub fn get(&self, trail: &Trail, index: usize) -> P::Val {
        self.pointer.get(&trail.trailed_mem, index)
    }

    #[inline(always)]
    pub fn set(&self, trail: &mut Trail, index: usize, val: P::Val) {
        self.pointer.set(&mut trail.trailed_mem, index, val);
    }

    #[inline(always)]
    pub fn swap(&self, trail: &mut Trail, index0: usize, index1: usize) {
        self.pointer.swap(&mut trail.trailed_mem, index0, index1);
    }
}

#[derive(Clone, Copy)]
pub struct StoredValue<P> {
    pointer: P,
}

impl<P> StoredValue<P>
where
    P: Pointer,
{
    pub fn new(builder: &mut TrailBuilder, val: P::Val) -> Self {
        StoredValue {
            pointer: P::new(&mut builder.stored_mem, val),
        }
    }

    #[inline(always)]
    pub fn get(&self, trail: &Trail) -> P::Val {
        self.pointer.get(&trail.stored_mem)
    }

    #[inline(always)]
    pub fn set(&self, trail: &mut Trail, val: P::Val) {
        self.pointer.set(&mut trail.stored_mem, val)
    }
}

#[derive(Clone, Copy)]
pub struct StoredArray<P> {
    pointer: P,
}

impl<P> StoredArray<P>
where
    P: ArrayPointer,
{
    pub fn new(builder: &mut TrailBuilder, vals: impl IntoIterator<Item = P::Val>) -> Self {
        StoredArray {
            pointer: P::new(&mut builder.stored_mem, vals),
        }
    }

    #[inline(always)]
    pub fn len(&self) -> usize {
        self.pointer.len()
    }

    #[inline(always)]
    pub fn get(&self, trail: &Trail, index: usize) -> P::Val {
        self.pointer.get(&trail.stored_mem, index)
    }

    #[inline(always)]
    pub fn set(&self, trail: &mut Trail, index: usize, val: P::Val) {
        self.pointer.set(&mut trail.stored_mem, index, val)
    }

    #[inline(always)]
    pub fn swap(&self, trail: &mut Trail, index0: usize, index1: usize) {
        self.pointer.swap(&mut trail.stored_mem, index0, index1);
    }
}

#[cfg(test)]
mod benches {
    extern crate test;

    use super::*;

    const SIZE: usize = 20000;

    macro_rules! bench_get_set {
        ( $TrailedT:ident, $get_fn_name:ident, $set_fn_name:ident ) => {
            #[bench]
            fn $get_fn_name(b: &mut test::Bencher) {
                let data = (0..SIZE).map(|_| rand::random()).collect::<Vec<_>>();

                let mut builder = TrailBuilder::new();
                let trailed_vals = (0..SIZE)
                    .zip(data.iter())
                    .map(|(_, &datum)| $TrailedT::new(&mut builder, datum))
                    .collect::<Vec<_>>();
                let mut trail = builder.finish();

                b.iter(|| {
                    for trailed_val in &trailed_vals {
                        test::black_box(trailed_val.get(&mut trail));
                    }
                });
            }

            #[bench]
            fn $set_fn_name(b: &mut test::Bencher) {
                let data = (0..SIZE).map(|_| rand::random()).collect::<Vec<_>>();

                let mut builder = TrailBuilder::new();
                let trailed_vals = (0..SIZE)
                    .zip(data.iter())
                    .map(|(_, &datum)| $TrailedT::new(&mut builder, datum))
                    .collect::<Vec<_>>();
                let mut trail = builder.finish();

                b.iter(|| {
                    for (trailed_val, &datum) in trailed_vals.iter().zip(data.iter()) {
                        trailed_val.set(&mut trail, datum);
                    }
                });
            }
        };
    }

    bench_get_set!(StoredI32, get_i32, set_i32);
    bench_get_set!(StoredI64, get_i64, set_i64);
    bench_get_set!(StoredIsize, get_isize, set_isize);

    bench_get_set!(StoredU32, get_u32, set_u32);
    bench_get_set!(StoredU64, get_u64, set_u64);
    bench_get_set!(StoredUsize, get_usize, set_usize);
}
